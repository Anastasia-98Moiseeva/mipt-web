import React from 'react';
import './styles.css';


export class Dogs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: false,
        };
    }

    changeChecked=()=> {
        this.setState({checked: !this.state.checked})
    }

    render() {
        if (this.props.dog === this.props.right) {
            return (
                <div className={this.state.checked ?
                    'dog-item-right' :
                    'dog-item'}
                     onClick={this.changeChecked}>
                    {this.props.dog}
                </div>);
        } else {
            return (
                <div className={this.state.checked ?
                    'dog-item-wrong' :
                    'dog-item'}
                     onClick={this.changeChecked}>
                    {this.props.dog}
                </div>);
        }
    }
}