import React from "react";
import './styles.css'

class Info extends React.Component {
    render() {
        return (
            <div>
                <div className="Info">
                    <h1>Викторина: породы собак.</h1>
                    <p><u>Пройди тест и узнай насколько хорошо ты знаешь породы собак!</u></p>
                </div>
                <div className="Quiz">
                    <p>Правильные ответы подсвечиваются зелёным цветом, неправильные - красным</p>
                    <p>Собака какой породы изображена на картинке?</p>
                </div>
            </div>

        );
    }


}

export default Info