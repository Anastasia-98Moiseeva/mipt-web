import React from "react";
import './App.css';
import Info from "./components/info";
import {Dogs} from "./components/dogs";

import BultererImage from './components/images/img/bulterer.jpg';
import GreyhaundImage from './components/images/img/greyhaund.jpg';
import SibaInuImage from './components/images/img/siba-inu.jpg';
import KorgyImage from './components/images/img/korgy.jpg';
import ZolotRetriverImage from './components/images/img/zolotistyi-retriver.jpg';

import {Images} from "./components/images";
import InfoSearch from './components/infoSearch';
import Search from "./components/search";
import Print from "./components/printImage";

const API_KEY = "496b2d49-a690-4675-9bbc-2e59bbba996a";

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            images: [
                {
                    id: 1,
                    src: BultererImage,
                    alt: "dog",
                    right: "Бультерьер",
                },
                {
                    id: 2,
                    src: GreyhaundImage,
                    alt: "dog",
                    right: "Грейхаунд",
                },
                {
                    id: 3,
                    src: SibaInuImage,
                    alt: "dog",
                    right: "Сиба-ину",
                },
                {
                    id: 4,
                    src: KorgyImage,
                    alt: "dog",
                    right: "Вельш-корги",
                },
                {
                    id: 5,
                    src: ZolotRetriverImage,
                    alt: "dog",
                    right: "Золотистый ретривер",
                }
            ],

            breeds: [
                {
                    id: 1,
                    dog: "Сибирский хаски",
                },
                {
                    id: 2,
                    dog: "Бишон фризе",
                },
                {
                    id: 3,
                    dog: "Вельш-корги",
                },
                {
                    id: 4,
                    dog: "Сиба-ину",
                },
                {
                    id: 5,
                    dog: "Уиппет",
                },
                {
                    id: 6,
                    dog: "Золотистый ретривер",
                },
                {
                    id: 7,
                    dog: "Грейхаунд",
                },
                {
                    id: 8,
                    dog: "Ши-тцу",
                },
                {
                    id: 9,
                    dog: "Акита-ину",
                },
                {
                    id: 10,
                    dog: "Бультерьер",
                },
            ],

            check: [
                {
                    id: 1,
                    str: "Проверить",
                }
            ],

            message: undefined,
            dogImage: undefined,
        }
    }

    gettingBreed = async (e) => {
        e.preventDefault();
        const breed = e.target.elements.breed.value;
        const api_url = await
            fetch(`https://api.thedogapi.com/v1/breeds/search?q=${breed}&api_key=${API_KEY}`);
        const data = await api_url.json();
        const dId = data[0].id;

        const new_api_url = await
            fetch(`https://api.thedogapi.com/v1/images/search?breed_ids=${dId}&api_key=${API_KEY}`);
        const new_data = await new_api_url.json();


        this.setState({
            dogImage: new_data[0].url,
        })
    }


    render() {
        return <div className="App">
            <Info/>
            {this.state.images.map((item1, index1) =>
                <p>
                    <Images key={index1}
                            src={item1.src}
                            alt={item1.alt}/>

                        {this.state.breeds.map((item2, index2) =>

                            <Dogs key={index2}
                                  dog={item2.dog}
                                  right={item1.right}/>)}
                        <hr/>
                </p>)}

            <InfoSearch/>
            <Search getBreed={this.gettingBreed}/>
            <Print img={this.state.dogImage}/>
            <hr/>
        </div>
    }
}

export default App;